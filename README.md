# Description

Scripts used in Corcoran et al., Archaic humans have contributed to large-scale variation in modern human T cell receptor genes, Immunity (2023), https://doi.org/10.1016/j.immuni.2023.01.026

# Dependencies

[IgDiscover TCR version](https://gitlab.com/gkhlab/igdiscover22/tree/TCR)<br />
Python (version 3.7.4), utilizing the following packages: pandas (version 1.1.4), scipy (version 1.4.1), numpy (version 1.18.0), Biopython (version 1.79), scikit-allel (version 1.3.3), pyfaidx (version 0.5.9.2), gtfparse (version 1.2.0), pyVCF (version 0.6.8), and seaborn (version 0.11.0). <br />
R (version 4.1.3), utilizing the following packages: ggplot2 (version 3.1.0), ComplexHeatmap (version 2.9.3), dplyr (version 1.0.9). <br />
[Sprime](https://github.com/browning-lab/sprime) (version 20May22.855). <br />
bcftools (version 1.16) <br />
[EIGENSOFT](https://github.com/DReichLab/EIG) (version 7.2.1) <br />

# Analysis Scripts

1. ```snp_table.py``` counts the number of haplotypes in individuals of 1KGP populations containing TCR allele SNPs (Supplementary table 4).
2. ```fst_wrapper.py``` is a wrapper to help run Fst calculations (see commands below).
3. ```sprime_wrapper.py``` is a wrapper to help run Sprime calculations (see commands below).
4. ```sprime_tract_analysis.py``` characterizes Sprime tracts overlapping with regions of interest. 
5. ```1KGP_ancient_pcas.py``` performs PCA of ancient TCR regions (Figure 6A).
6. ```merge_reads_split_TCR_chains.py``` merges paired end libraries and splits by TCR chain.
7. ```genotype.ipynb``` compiles IgDiscover, corecount, and Sanger validation. 
8. ```haplotype.ipynb``` performs haplotyping analysis (Supplementary table 3).

# Plotting scripts

1. ```plot_heatmaps.R``` plots haplotype and allele presence heatmaps (Fig. 2, Fig. 3, Supplementary Fig. 3).
2. ```plot_population_SNP_PCA.R``` plots PCA of combined donor and 1KGP allele presence (Fig. 4D). 
3. ```plot_ancient_alleles_in_1KGP_barplot.py``` plots a barplot of the counts of three ancient TCR alleles in 1KGP populations (Fig. 5B). 
4. ```plot_Fst.R``` plots windowed Fst between individuals in same population, divided by presence of ancient alleles (Fig. 5C)

# Ancient genomes analysis
## To download ancient VCFs: 

1. wget the chr7 and chr14 data from http://ftp.eva.mpg.de/neandertal/
2. tabix -p vcf vcf.tar.gz everything 
3. softlink to chr{}.vcf.gz to simplify access


## Generate Fst values between individuals with and without introgressed alleles within each population (fig 5C)

```
# For TRGV4*02_S0072:
python fst_wrapper.py --within-each-population --prefix TRGV4 ../data/tg/integrated_call_samples_v3.20130502.ALL.panel ../data/tg/all_tg_TRGV4_S0072_names.csv ../data/tg/chr7/ALL.chr7.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz ../data/tg/fsts/

# For TRAV12-2*02_S6060:
python fst_wrapper.py --within-each-population --prefix TRAV12-2 ../data/tg/integrated_call_samples_v3.20130502.ALL.panel ../data/tg/all_tg_TRAV12-2_names.csv ../data/tg/chr14/ALL.chr14.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz ../data/tg/fsts/

# For TRAJ24\*02_S1049/TRAJ26\*01_S5236 (the sample list containing these  alleles is the same for both):
python fst_wrapper.py --within-each-population --prefix TRAJ24 ../data/tg/integrated_call_samples_v3.20130502.ALL.panel ../data/tg/all_tg_TRAJ24_names.csv ../data/tg/chr14/ALL.chr14.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz ../data/tg/fsts/
```

## Commands to run Sprime on each of the 1KGP populations:

```
# chromosome 7:
python sprime_wrapper.py --outgroup-pop YRI ../data/tg/integrated_call_samples_v3.20130502.ALL.panel ~/software/plink_GRCh37/plink.chr7.GRCh37.map ../data/tg/chr7/ALL.chr7.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz ../data/tg/sprime/yri/chr7/

# chromosome 14:
python sprime_wrapper.py --outgroup-pop YRI ../data/tg/integrated_call_samples_v3.20130502.ALL.panel ~/software/plink_GRCh37/plink.chr14.GRCh37.map ../data/tg/chr14/ALL.chr14.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz ../data/tg/sprime/yri/chr14/
```

## To generate the introgression PCA (Figure 6A)

### 1. Subset 1KGP and ancient VCFs for introgressed regions, then concatenate. Use bcftools.

### 2. Convert vcf to eigeinstrat using gdc https://github.com/mathii/gdc 
```
python2  /home/mchernys/software/gdc/vcf2eigenstrat.py -v  ../data/tg/vcf/TRGV4\*02_S0072_38383867-38419878_GRCh37_VD.vcf.gz -o ../data/tg/PCAproj/TRGV4\*02_S0072_38383867-38419878_GRCh37_VD
python2  /home/mchernys/software/gdc/vcf2eigenstrat.py -v  ../data/tg/vcf/TRAV12-2\*02_S6060_22350182-22379420_GRCh37_VD.vcf.gz -o ../data/tg/PCAproj/TRAV12-2\*02_S6060_22350182-22379420_GRCh37_VD
python2  /home/mchernys/software/gdc/vcf2eigenstrat.py -v  ../data/tg/vcf/TRAJ24\*02_S1049_22981802-23019943_GRCh37_VD.vcf.gz -o ../data/tg/PCAproj/TRAJ24\*02_S1049_22981802-23019943_GRCh37_VD
```

### 3. Run smartpca from EIG https://github.com/DReichLab/EIG

```
# to run all introgressed region PCAs for all 1KGP populations
python3 tg_population_pcas.py
```

# IgDiscover analysis

## 1. Run IgDiscover

```
# database sequences can be obatined from https://gkhlab.gitlab.io/tcr/sequences/
db_dir=../data/KITDB
# to run the newer version, IgDiscover22 v1.0.0, set yaml_folder=./igdiscover_yamls/IgDiscover22_v1/ 
yaml_folder=./igdiscover_yamls/TCR/
# TRAD db is the same as TRA (these libraries were created for improved detection of TRADV alleles)
chains=( TRA TRAD TRB TRD TRG )
for chain in "${chains[@]}"; 
do 
    echo cp ./data/V.tsv ${db_dir}/${chain}/V.tsv # V length file corrects lengths
    echo cp ./data/human_gl.aux ${db_dir}/${chain}/human_gl.aux # aux file obtained from IgBLAST website for better CDR3 detection
    echo igdiscover batch igdiscover --igdiscover-yaml ${yaml_folder}/${chain}.yaml --database ${db_dir}/${chain} ../data/libraries/${chain} ../data/igdiscover/${chain}; 
done
```

## 2. Run corecount

```
# Vs
chains=( TRA TRAD TRB TRD TRG )
for chain in "${chains[@]}"; do echo igdiscover batch corecount -e 5 -g V -x -s genomic_sequence -l 1.1 -r 0.05 -u iteration-01/Vcore_filtered_uout.tab database/V.fasta iteration-01/filtered.tab.gz iteration-01/Vcore.tab ../data/igdiscover/${chain}; done

# Ds
chains=( TRB TRD )
for chain in "${chains[@]}"; do echo igdiscover batch corecount -g D -x -l 1.1 -r 0.05 database/D.fasta iteration-01/filtered.tab.gz iteration-01/Dcore.tab ../data/igdiscover/${chain}; done

# Js
chains=( TRA TRB TRD TRG )
for chain in "${chains[@]}"; do echo igdiscover batch corecount -b 6 -g J -x -s genomic_sequence -l 1.1 -r 0.05 database/J.fasta iteration-01/filtered.tab.gz iteration-01/Jcore.tab ../data/igdiscover/${chain}; done

# run on assigned.tab files to find stop codon alleles (ENFs)
igdiscover batch corecount -g V -u iteration-01/Vcore_assigned_uout.tab database/V.fasta iteration-01/assigned.tab.gz iteration-01/Vcore_assigned.tab ../data/igdiscover/TRA
igdiscover batch corecount -g V -u iteration-01/Vcore_assigned_uout.tab database/V.fasta iteration-01/assigned.tab.gz iteration-01/Vcore_assigned.tab ../data/igdiscover/TRB
igdiscover batch corecount -g J -u iteration-01/Jcore_assigned_uout.tab database/J.fasta iteration-01/assigned.tab.gz iteration-01/Jcore_assigned.tab ../data/igdiscover/TRA
```


## 3. Collect results

```
igdiscover_run_dir=../data/igdiscover
files=( annotated_V_germline expressed_D expressed_J Vcore Dcore Jcore Vcore_filtered_uout Jcore_assigned_uout Vcore_assigned_uout)
for file in "${files[@]}"; do echo igdiscover batch collect --file iteration-01/${file}.tab ${igdiscover_run_dir} ${igdiscover_run_dir}/collected/collected_${file}.tab ; done
```

## 3. Post-processing and haplotyping in genotype.ipynb and haplotype.ipynb
