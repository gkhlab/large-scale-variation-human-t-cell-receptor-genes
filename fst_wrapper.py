import os
import re
from multiprocessing import Pool
import subprocess
import pandas as pd
from collections import Counter
import argparse
"""
Wrapper for running vcftools fst calculation. 
Calculates fst values between 1KGP individuals split on introgressed allele presence. Runs one population at a time by default. 
"""

def add_arguments(parser):
    arg = parser.add_argument
    arg('population_data', type=str,
        help="""Tab delimited population information on VCF samples. Requires 'sample' and 'pop' columns."""
            """ For 1KGP, can be obtained here http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/""")
    arg('group1_samples', type=str,
        help="""A file containing a newline delimited list of samples (group1) to compare with the rest of the samples in the file (group2)."""
            """ For this paper, it is the list of individuals containing introgressed alleles. This list is generated in """
            """presence_in_ancients.ipynb""")
    arg('vcf', type=str,
        help="""1KGP phased VCF file. Can be found at http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/.""")
    arg('out_dir', type=str,
        help="Folder where to place the output fst files.")
    arg('--within-each-population', action='store_true', default=False,
        help="""Whether to compare individuals within and outside of group1_samples per population."""
            """ Mutually exclusive with --compare-each-populations""")
    arg('--compare-each-population', action='store_true', default=False,
        help="""Whether to compare members of group1 to each population separately."""
            """ Mutually exclusive with --within-population""")
    arg('--maf', type=float, default=.05,
        help='Minimum minor allele frequency to include a SNP position.')
    arg('--exclude', type=str, default="",
        help='Individuals to exclude from the fst calculation entirely.')
    arg('--vcftools-bin', type=str, default='/usr/local/bin/vcftools',
        help='Path to vcftools binary. Obtain over here: https://vcftools.github.io/downloads.html')
    arg('--n-pooled', type=str, default=20,
        help='Number of fst calculation commands to run simultaneously.')
    arg('--prefix', type=str, default = "",
        help="""Prefix to add to output files. For instance, which allele distinguishes between the groups being compared.""")

parser = argparse.ArgumentParser()
add_arguments(parser)
args = parser.parse_args()

tg_positive_names = pd.read_csv(args.group1_samples).iloc[:,0].values
tg_pop_df = pd.read_table(args.population_data)

if args.within_each_population or args.compare_each_population:
    pos_dict = dict()
    neg_dict = dict()
    if args.within_each_population:
        all_positives_df = tg_pop_df[tg_pop_df['sample'].isin(tg_positive_names)]
        for pop, pop_df in all_positives_df.groupby(by = 'pop'):
            pos_dict[pop] = list(pop_df['sample'].values)
            neg_dict[pop] = list(set(tg_pop_df[tg_pop_df['pop'].eq(pop)]['sample'].values).difference(set(pop_df['sample'].values)))
    elif args.compare_each_population:
        for pop, pop_df in tg_pop_df.groupby(by = 'pop'):
            neg_dict[pop] = list(pop_df['sample'].values)
            pos_dict[pop] = tg_positive_names
            
    #2. all to all pop1s vs pop2s fst finding 
    #A. loop thru populations to create path pairs
    path_groups = []
    for pop in list(pos_dict.keys()):
        pos_path = f"{args.out_dir}{args.prefix}_{pop}_pos.csv"
        neg_path = f"{args.out_dir}{args.prefix}_{pop}_neg.csv"
        pd.DataFrame({'A' : pos_dict[pop]}).to_csv(pos_path, header = False, index = False)
        pd.DataFrame({'A' : neg_dict[pop]}).to_csv(neg_path, header = False, index = False)
        out_path = f"{args.out_dir}{args.prefix}_{pop}"
        path_groups = path_groups + [[args.vcf, pos_path, neg_path, out_path]]
else:
    # individuals not in positive list
    neg_list = list(set(tg_pop_df['sample'].values).difference(set(tg_positive_names)))
    if args.exclude != "":
        exclude_list = pd.read_csv(args.exclude).iloc[:,0].values
        neg_list = list(set(neg_list).difference(set(exclude_list)))
        
    neg_path = os.path.join(args.out_dir,  f"{args.prefix}_neg.csv")
    pd.DataFrame({'A' : neg_list}).to_csv(neg_path, header = False, index = False)
    out_path = os.path.join(args.out_dir, args.prefix)
    path_groups = [[args.vcf, args.group1_samples, neg_path, out_path]]
    
    
def start_fst(path_group):
    command_str = " ".join([args.vcftools_bin,
                '--gzvcf', path_group[0],
                '--maf', str(args.maf),
                '--weir-fst-pop', path_group[1],
                '--weir-fst-pop', path_group[2],
                '--out', path_group[3] + "_maf05"])
    print(command_str)
    subprocess.call(command_str, shell=True)
    
#B. start fst commands
p = Pool(args.n_pooled)
p.map(start_fst, path_groups)
