import pandas as pd 
import numpy as np
import os
import allel
from scipy import stats
from tqdm import tqdm
from collections import Counter
import pyfaidx as px


"""
Counts the number of haplotypes in individuals of 1KGP populations containing TCR allele SNPs (Supplementary table 4).
"""

# inputs
# TCR allele SNPs
SNP_path = "./data/TCR_variants.tsv"
# .tsv file w 2 columns: case and population
KI_populations_path = "./data/case2population.tsv"
# .tsv file w case and sequence_id columns 
KI_genotypes_path = os.path.join("../outputs/validations_and_genotypes/long_genotypes.tsv")
# 1KGP population informatonn table, which can be obtained here https://www.internationalgenome.org/data-portal/sample
tg_pop_infopath = os.path.join("./data/igsr_samples.tsv")

# 1KGP GRCh38 VCF file only has positions, not rsids, so I must translate
# translation obtained by quering rsids on the VEP web interface at http://www.ensembl.org/Tools/VEP
# I was having problems with 503 responses to my VEP API requests...
# {assembly} is replaced by 37 and 38 
VEP_path = "../outputs/snp_frequency/TCR_rsids_VEP_{assembly}.tsv"

# I subset 1KGP VCFs for TCR loci so I can read them into memory in in their entirety
tg_VCF_path = "../data/tg/chr{chromosome}/chr{chromosome}_TR{loci}_{assembly}.vcf.gz"


variant_cols = 14

chain2chromosome = {"TRA" : "14",
                    "TRB" : "7",
                    "TRD" : "14",
                    "TRG" : "7"}

rsids = [row["SNP_" + str(q + 1)] for row in pd.read_table(SNP_path).to_dict("records") for q in range(variant_cols) if isinstance(row["SNP_" + str(q + 1)], str) ]

pop_df = pd.read_table(tg_pop_infopath)
#sample2pop = dict(zip(pop_df['sample'].values, pop_df["pop"].values))
sample2pop = dict(zip(pop_df['Sample name'].values, pop_df["Population code"].values))
pop2region = dict(zip(pop_df['Population code'].values, pop_df["Superpopulation code"].values))


"""
Count occurences of SNP haplotypes
"""

snps_not_found = dict()
superpop_totals = dict()
combined_snp_df = pd.DataFrame()
long_results = []
for assembly in [37, 38]:
    print(assembly)

    # read in 1KGP VCFs and map rsids to positions since 38th assembly VCF has no variant IDs
    vcfs = dict()
    for chromosome in ["7", "14"]:
        if chromosome == '7':
            loci = 'BG'
        elif chromosome == '14':
            loci = 'AD'
        callset = allel.read_vcf(tg_VCF_path.format(chromosome = chromosome, loci = loci, assembly = assembly))
        vcfs[chromosome] = callset
    superpop_totals.update(Counter([f"TG_{pop2region[sample2pop[el]]}_{assembly}" for el in pd.unique(vcfs['14']['samples']) if el != 'NA12236' and el != 'HG01783']))
    if assembly == 37:
        rsid2pos = dict()
        for chromosome in ["7", "14"]:
            rsid2pos.update(dict(zip(vcfs[chromosome]['variants/ID'], vcfs[chromosome]['variants/POS'])))
    elif assembly == 38:
        # rsid to position translation for 38th assembly VCFs which don't have rsids
        rsids_vep = pd.read_table(VEP_path.format(assembly = assembly))
        rsids_vep = rsids_vep.drop_duplicates(subset = "#Uploaded_variation").reset_index(drop = True)
        rsids_vep["position"] = rsids_vep["Location"].apply(lambda x: x.split("-")[-1])
        rsids_vep["chromosome"] = rsids_vep["Location"].apply(lambda x: x.split(":")[0])
        rsid2pos = dict(zip(rsids_vep["#Uploaded_variation"].values, rsids_vep["position"].values))
    def to_position(el, rsid2pos):
        # some SNPs don't have an rsID, so they were input as coordinates
        if el in rsid2pos:
            return int(rsid2pos[el])
        elif "chr" in el:
            return int(el.split(":")[-1].replace(",", ""))
        else:
            return el

    # the SNPs we are searching for
    snp_df = pd.read_table(SNP_path)
    snp_df['chain'] = snp_df.allele.apply(lambda x : x[:3])
    snp_df['tg'] = ""
    snp_df['chromosome'] = snp_df['chain'].apply(lambda x: chain2chromosome[x])
    not_all_rsids_found = []

    # find 1KGP samples with matching SNPs in same haplotype
    for row in tqdm(snp_df[snp_df['1000_G'].eq('YES')].reset_index().to_dict('records')):
        i = row["index"]
        chromosome = str(row['chromosome'])
        
        # set of SNPs for this allele
        positions = [to_position(row["SNP_" + str(q + 1)], rsid2pos) for q in range(variant_cols) if isinstance(row["SNP_" + str(q + 1)], str)]
        alts = [row["variant_" + str(q + 1)] for q in range(variant_cols) if isinstance(row["variant_" + str(q + 1)], str)]

        # deletions are represented as empty strings
        alts = [alt if alt != '-' else ''  for alt in alts]
        inds = [np.where(vcfs[chromosome]['variants/POS'] == (position))[0][0] for position in positions if position in vcfs[chromosome]['variants/POS']]
        if len(inds) == len(positions) and len(inds) == len(alts):
            #haplotypes for each individual
            for q in range(len(vcfs[chromosome]['samples'])):
                for hap_ind in range(2):
                    hap = True
                    # if genotype doesn't match rsid list, the haplotype does not contain the allele
                    for rsid_ind in range(len(inds)):
                        gt = vcfs[chromosome]['calldata/GT'][inds[rsid_ind]][q][hap_ind]
                        if gt == 0:
                            if vcfs[chromosome]['variants/REF'][inds[rsid_ind]] != alts[rsid_ind]:
                                hap = False
                        elif vcfs[chromosome]['variants/ALT'][inds[rsid_ind]][gt - 1] != alts[rsid_ind]:
                            hap = False
                    if hap == True:
                        if snp_df.loc[i, 'tg'] == "": 
                            snp_df.loc[i, 'tg'] = vcfs[chromosome]['samples'][q]
                        else:
                            snp_df.loc[i, 'tg'] = f"{snp_df.loc[i, 'tg']} {vcfs[chromosome]['samples'][q]}"
                        long_results.append((row['allele'], vcfs[chromosome]['samples'][q], str(assembly)))
        else:
            not_all_rsids_found =  not_all_rsids_found + [row['allele']]
            print("WARNING: NOT ALL RSIDS FOUND FOR " + row['allele'])

    snp_df['assembly']=assembly
    combined_snp_df = pd.concat([combined_snp_df, snp_df])
    snps_not_found[assembly] = not_all_rsids_found
    print("NOT ALL SNPS FOUND FOR " + str(len(not_all_rsids_found)) + " ALLELES")


combined_snp_df = combined_snp_df.reset_index(drop=True)
# I checked both haplotypes, so remove duplicate results here
combined_snp_df['tg'] = combined_snp_df['tg'].apply(lambda x: " ".join(pd.unique(x.split(" "))))

TG_populations = ["AFR", "SAS", "EAS", "EUR", "AMR"]
KI_populations = ["AFR", "SAS", "EAS", "EUR"]
KI_long_genotypes = pd.read_csv(KI_genotypes_path, sep = '\t')
case2population_df = pd.read_table(KI_populations_path)
case2population = dict(zip(case2population_df["case"].values, case2population_df["population"].values))

# KI superpops totals and per allele superpops
for pop in KI_populations:
    superpop_totals[f'KI_{pop}'] = 0
    combined_snp_df[f'KI_{pop}'] = 0

superpop_totals.update(Counter([f"KI_{case2population[case]}" for case in KI_long_genotypes.case.unique() if "T" not in case]))

for allele, df in KI_long_genotypes[~KI_long_genotypes.case.str.contains('T')].groupby(by = "sequence_id"):
    ctr = Counter([case2population[case] for case in df.case.values])
    for k in ctr.keys():
        combined_snp_df.loc[combined_snp_df.allele.eq(allele), 'KI_' + k] = ctr[k]  

# get KI case counts per allele
for allele, df in combined_snp_df.groupby("allele"):
    cases = KI_long_genotypes.loc[KI_long_genotypes.sequence_id.eq(allele), 'case'].unique()
    for case in cases:
        if "T" not in case:
            long_results.append((allele, case, ""))

for pop in TG_populations:
    combined_snp_df[f'TG_{pop}'] = 0

# get 1KGP population counts per allele
for row in tqdm(combined_snp_df.query("tg != ''").reset_index().to_dict('records')):
    for q in pd.unique(row["tg"].split(" ")):
        if q != 'NA12236' and q != 'HG01783': # problem individuals with missing or multiple populations
            combined_snp_df.loc[row['index'], f"TG_{pop2region[sample2pop[q]]}"] = combined_snp_df.loc[row['index'], f"TG_{pop2region[sample2pop[q]]}"] + 1            


"""
Generate long version of the dataframe for visualizing population on our website
"""

cols = ["chromosome", "chain", "gene", "family", "allele", "population", "sample", "experiment", "assembly", "snp", "variant"]
long_snp_df = pd.DataFrame(long_results, columns = ["sequence_id", "sample", "assembly"])
long_snp_df = long_snp_df.drop_duplicates(subset = ["sequence_id", "sample", "assembly"]).reset_index(drop = True)
# add extra info columns
long_snp_df = long_snp_df.loc[(long_snp_df['sample'] != "NA12236") & (long_snp_df['sample'] != "HG01783"),:].reset_index(drop = True)
long_snp_df["assembly"] = long_snp_df["assembly"].apply(lambda x: f"GRCh{x}" )
long_snp_df.loc[long_snp_df["sample"].apply(lambda x: "D"  in x), "assembly"] = "" 
long_snp_df['chain'] = long_snp_df.sequence_id.apply(lambda x : x[:3])
long_snp_df['gene'] = long_snp_df.sequence_id.apply(lambda x : x[3])
def allele2family(allele): 
    if "site" in allele:
        return ""
    else:
        return allele.split("*")[0][4:]
long_snp_df['family'] = long_snp_df.sequence_id.apply(allele2family)
long_snp_df["allele"] = long_snp_df["sequence_id"].apply(lambda x: x.split("*")[-1])
long_snp_df["experiment"] = "GENOMES"
long_snp_df.loc[long_snp_df["sample"].apply(lambda x: "D" in x), "experiment"] = "KI"
chain2chromosome = {"TRA" : "14",
                    "TRB" : "7",
                    "TRD" : "14",
                    "TRG" : "7"}
long_snp_df["chromosome"] = long_snp_df["chain"].apply(lambda x : chain2chromosome[x])
# population information
long_snp_df.loc[long_snp_df["sample"].apply(lambda x: "D" not in x), "population"] = long_snp_df.loc[long_snp_df["sample"].apply(lambda x: "D" not in x), "sample"].apply(lambda x : pop2region[sample2pop[x]])
long_snp_df.loc[long_snp_df["sample"].apply(lambda x: "D" in x), "population"] = long_snp_df.loc[long_snp_df["sample"].apply(lambda x: "D" in x), "sample"].apply(lambda x : case2population[x])
# sequences
# allele rsids 
allele2rsids = dict()
allele2variants = dict()
for i in snp_df.index.values:
    rsids = [snp_df.loc[i, "SNP_" + str(q + 1)] for q in range(variant_cols) if isinstance(snp_df.loc[i, "SNP_" + str(q + 1)], str)]
    rsids = [rsid.replace(",", "") for rsid in rsids]
    variants = [snp_df.loc[i, "variant_" + str(q + 1)] for q in range(variant_cols) if isinstance(snp_df.loc[i, "variant_" + str(q + 1)], str)]
    allele2rsids[snp_df.loc[i, "allele"]] = rsids
    allele2variants[snp_df.loc[i, "allele"]] = variants
long_snp_df["snp"] = long_snp_df.sequence_id.apply(lambda x: ",".join(allele2rsids[x]))
long_snp_df["variant"] = long_snp_df.sequence_id.apply(lambda x: ",".join(allele2variants[x]))
long_snp_df[cols].to_csv("../outputs/snp_frequency/long_1KGP_SNPs.tsv", sep = '\t', index = False)

"""
Find outliers
"""

# get bonferroni correction 
exclude_inds = []
tg_bonferroni_correction = dict()
for assembly in [37, 38]:
    for row in combined_snp_df[combined_snp_df.assembly.eq(assembly)].to_dict("records"):
            if np.all([row[el] for el in ["TG_AFR", "TG_SAS", "TG_EAS", "TG_EUR", "TG_AMR"]] == [0,0,0,0,0]):
                exclude_inds = exclude_inds + [i]
    include_list = list(set(combined_snp_df.index.values).difference(set(exclude_inds)))
    tg_bonferroni_correction[assembly] = len(include_list) * 5
    

tot_tg_individuals = dict()
tot_tg_individuals[37] = 2504
tot_tg_individuals[38] = 3202
tot_ki_individuals = 45

for population in TG_populations:
    combined_snp_df[f"1000 GP {population} adjusted p-value"] = -1

for assembly in [37, 38]:
    for i in combined_snp_df[combined_snp_df.assembly.eq(assembly)].index.values:
        tg_row_tot = np.sum([combined_snp_df.loc[i, "TG_" + population] for population in TG_populations])
        for population in TG_populations:
            p = (superpop_totals["TG_" + population + "_" + str(assembly)] / tot_tg_individuals[assembly]) * (tg_row_tot / tot_tg_individuals[assembly])
            pval = stats.binom_test(combined_snp_df.loc[i, "TG_" + population],
                                                                           n = tot_tg_individuals[assembly],
                                                                           p = p, 
                                                                           alternative = "two-sided" ) * tg_bonferroni_correction[assembly]
            pval = np.min([1, pval])
            combined_snp_df.loc[i, f"1000 GP {population} adjusted p-value"] = pval


"""
Convert to wide format for paper
"""

temp_df = combined_snp_df.sort_values(by = ['allele', 'assembly']).reset_index(drop = True).copy()
def get_1KGP_freq(row, population, superpop_totals):
    if row["1000_G"] == "YES":
        ct = row[f"TG_{population}"]
        p = f"TG_{pop}_{str(row['assembly'])}"
        return f"{ct} ({round(ct/ superpop_totals[p], 3)})"
    else:
        return "N.D."

for assembly in [37, 38]:
    for pop in TG_populations:
        temp_df.loc[temp_df.assembly.eq(assembly), f'1000 GP {pop} count (frequency)'] = temp_df.loc[temp_df.assembly.eq(assembly),].apply(lambda row: get_1KGP_freq(row, pop, superpop_totals), axis = 1)

for pop in KI_populations:
    temp_df = temp_df.rename(columns = {f"KI_{pop}" : f"Donors {pop} count (frequency)"})

    temp_df[f"Donors {pop} count (frequency)"] = temp_df[f"Donors {pop} count (frequency)"].apply(lambda x: f"{x} ({round(x/ superpop_totals[f'KI_{pop}'], 3)})")

temp_df['gene'] = temp_df['allele'].apply(lambda x: x.split("*")[0])
temp_df = temp_df.rename(columns = {"1000_G" : "1000 GP result", 
                                "tg" : "1000 GP Ids"})
#reorder columns
cols = "allele	chromosome	gene	1000 GP result	1000 GP Ids	assembly	Donors AFR count (frequency)	1000 GP AFR count (frequency)	Donors SAS count (frequency)	1000 GP SAS count (frequency)	Donors EAS count (frequency)	1000 GP EAS count (frequency)	Donors EUR count (frequency)	1000 GP EUR count (frequency)	1000 GP AMR count (frequency)	1000 GP AFR adjusted p-value	1000 GP SAS adjusted p-value	1000 GP EAS adjusted p-value	1000 GP EUR adjusted p-value	1000 GP AMR adjusted p-value	SNP_1	variant_1	SNP_2	variant_2	SNP_3	variant_3	SNP_4	variant_4	SNP_5	variant_5	SNP_6	variant_6	SNP_7	variant_7	SNP_8	variant_8	SNP_9	variant_9	SNP_10	variant_10	SNP_11	variant_11	SNP_12	variant_12	SNP_13	variant_13".split("\t")
temp_df = temp_df.loc[:,cols]
# wide tsv
temp_df.to_csv(os.path.join("./data/snp_pop_freqs_both_assemblies.tsv"), sep = '\t', index = False)


# wide xlsx
with pd.ExcelWriter(os.path.join("./data/snp_pop_freqs_both_assemblies.xlsx")) as writer:  # doctest: +SKIP
    for pair in [("TRAV", "TRAV"), ("TRAJ", "TRAJ"), 
                    ("TRBV", "TRBV"), ("TRBD", "TRBD"), ("TRBJ", "TRBJ"),
                    ("TRGV", "TRGV"), ("TRGJ", "TRGJ"),
                    ("TRDV", "TRDV"), ("TRDD", "TRDD"), ("TRDJ", "TRDJ")]:
        temp_df[temp_df.allele.apply(lambda x: pair[0] in x)].to_excel(writer, sheet_name=pair[1])
