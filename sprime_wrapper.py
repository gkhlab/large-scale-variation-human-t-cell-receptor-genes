import os
import re
from multiprocessing import Pool
import subprocess
import pandas as pd
from collections import Counter
import numpy as np
import sys
import argparse

"""
Wrapper to run Sprime on each individual 1KGP subpopulations one chromosome at a time
"""

def add_arguments(parser):
    arg = parser.add_argument
    arg('population_data', type=str,
        help="""Tab delimited population information on VCF samples. Requires 'sample' and 'pop' columns."""
            """ For 1KGP, can be obtained here http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/""")
    arg('plink_map', type=str,
        help='Plink map.')
    arg('vcf', type=str,
        help="""1KGP phased VCF file. Can be found at http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/.""")
    arg('out_dir', type=str,
        help="Folder where to place the output Sprime files.")
    arg('--outgroup-pop', type=str, default = "YRI",
        help="Outgroup population.")
    arg('--populations', type=str, default="",
        help='Which populations to run on.')
    arg('--group-samples', type=str, default = "",
        help="""Run Sprime separately on samples inside and outside of the group. Used for running Sprime on the subset of individuals with introgressed alleles.""")
    arg('--exclude', type=str, default="",
        help='Individuals to exclude from the outgroup.')
    arg('--sprime-jar', type=str, default="~/software/bin/sprime.jar",
        help="jar file for Sprime software")
    arg('--by-individual', action="store_true", default=False,
        help="""Whether to make separate runs for outgroup vs each individual. The 
             default behaviour is to run Sprime once per 1KGP population""")
    arg('--num-pooled', default = 4, 
        help="number of commands to run simultaneously")

parser = argparse.ArgumentParser()
add_arguments(parser)
args = parser.parse_args()
tg_pop_df = pd.read_table(args.population_data)

if args.populations == "":
    populations = ["CHS"]
#    populations = ['FIN', 'PUR', 'CDX', 'CLM', 'IBS', 'CHS',
#                              'PEL', 'PJL', 'KHV', 'ACB', 'GWD', 
#                              'ESN', 'BEB', 'MSL', 'CEU', 'CHB',
#                              'JPT', 'ASW', 'MXL', 'TSI', 'GIH', 
#                              'LWK', 'ITU', 'STU', 'YRI', 'GBR']
else:
    populations = [el.replace(" ", "") for el in args.populations.split(",")]


pop_dict = dict()
for pop in populations:
    pop_all_samples = tg_pop_df.loc[tg_pop_df['pop'].eq(pop)]['sample'].values
    if args.group_samples == "":
        pop_dict[pop] = pop_all_samples
    else:
        pos_samples = pd.read_csv(args.group_samples).iloc[:,0].values
        pop_dict[f"{pop}_neg"] = list(set(pop_all_samples).difference(set(pos_samples)))
        pop_dict[f"{pop}_pos"] = list(set(pop_all_samples).intersection(set(pos_samples)))
        

# get individuals to use as part of the outgroup, excluding those with TRGV4_S0072 due to back migration
outgroup_individuals = tg_pop_df.loc[tg_pop_df['pop'].eq(args.outgroup_pop)]['sample'].values
if args.exclude != '':
    exclude_from_outgroup = pd.read_csv(args.exclude, sep = ',').iloc[:,0].values
    outgroup_individuals = list(set(outgroup_individuals).difference(exclude_from_outgroup))
df = pd.DataFrame({'A' : outgroup_individuals})
outgroup_path = os.path.join(args.out_dir, "outgroup.csv")
df.to_csv(outgroup_path, header = None, index = False)

all_phase3_individuals = tg_pop_df['sample'].values

pop_exclusion_path_dict = dict()
path_groups = []

# 1. go thru all populations 
for pop in list(pop_dict.keys()):
    if not args.by_individual:
        #2. add individuals from that population to the african list
        individuals_to_include = np.concatenate((outgroup_individuals, pop_dict[pop]))
        #3. get difference between all individuals and those from step 2 to get exclusion list. 
        individuals_to_exclude = list(set(all_phase3_individuals).difference(set(individuals_to_include)))
        to_exclude_path = os.path.join(args.out_dir, pop + "_to_exclude_sprime.csv")
        df = pd.DataFrame({'A' : individuals_to_exclude})
        df.to_csv(to_exclude_path, header = None, index = False)

        out_path =  os.path.join(args.out_dir,  pop + "_sprime")
        path_groups = path_groups + [[args.vcf, outgroup_path, args.plink_map, out_path, to_exclude_path ]]
    else:
        for individual in pop_dict[pop]:
            individuals_to_include = np.concatenate((outgroup_individuals, [individual]))
            individuals_to_exclude = list(set(all_phase3_individuals).difference(set(individuals_to_include)))
            to_exclude_path = os.path.join(args.out_dir, pop + "_individual_" + individual + "_to_exclude_sprime.csv")
            df = pd.DataFrame({'A' : individuals_to_exclude})
            df.to_csv(to_exclude_path, header = None, index = False)
            out_path =  os.path.join(args.out_dir,  pop + "_individual_" + individual + "_sprime")
            path_groups = path_groups + [[args.vcf, outgroup_path, args.plink_map, out_path, to_exclude_path ]]

def run_sprime(path_group):
    command_str = 'java -jar ' + args.sprime_jar + \
                ' gt=' + path_group[0] + \
                ' outgroup=' + path_group[1] + \
                ' map=' + path_group[2] + \
                ' out=' + path_group[3] + \
                ' excludesamples=' + path_group[4]
    print(command_str)
    subprocess.call(command_str, shell=True)
    
#B. start sprime commands
p = Pool(args.num_pooled)
p.map(run_sprime, path_groups)